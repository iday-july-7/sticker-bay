
# :pizza: Sticker Bay

`GitHub WARNING`: This is a __mirror repo__ from [GitLab](https://gitlab.com/distributopia/sticker-bay). Please, send your MRs there.

# Stickers - grab them and print them


#### Sticker Pack #1

![preview screen](stickerpack-1.png?raw=true)

##### Credits
These stickers use some vector graphics from Pixabay ( [1](https://pixabay.com/en/cute-elephants-elephant-balloons-2757831), [2](https://pixabay.com/en/gnu-africa-animal-face-head-159596), [3](https://pixabay.com/en/animal-cartoon-comic-equine-horse-2030012), [4](https://pixabay.com/en/dragon-pet-boy-domestication-152823), [5](https://pixabay.com/en/sos-devil-bull-47037) ), and [vectorportal](http://vectorportal.com).

##### Authors
[@lostinlight](https://libenter.win)

##### License
CC0


## Contributing

*Please, add your sticker packs!*
The more people contribute, the more different styles there will be.
Add a new folder (for example, `stickerpack-2`). Then add Sticker pack #2 (authors, license, credits if necessary, preview image if possible) to README.

If you modify existing stickers and believe your modifications make original image better, please, push those modifications back. And some explanation why you did that. Thank you!
